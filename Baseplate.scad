/*[Baseplate]*/
diameter = 200 ; // [100:1:200]
thickness = 10 ; //[5:0.1:20]

/*[Chimney]*/
chimney_diameter = 50 ;//[5:1:80]
chimney_height = 80 ;//[10:1:120]

 $fn=100; 
 
cylinder(h=thickness, d=diameter, center= true);
translate([0,0,thickness]) ; cylinder(h=chimney_height, d=chimney_diameter, center = false);

